import { useEffect, useState } from "react";




function App() {
  const [count, updateCount] = useState(0)
  const handleClick = () => {
    updateCount(count + 1)
  }

  useEffect(() => {
    document.title = `Bạn đã bấm ${count} lần`;
  });
  return (
    <div style={{margin:"0 200px",width:"20%", backgroundColor:"#001a33"}}>
      <p style={{color:"#ccffff", marginLeft:"20px"}}>You click {count} times</p>
      <button style={{backgroundColor:"#ccffff",marginLeft:"20px"}} onClick={handleClick}>Click</button>
    </div>
  );
}

export default App;
